import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Users } from 'src/app/models/users';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  data : Users[] = []

  constructor(private router : Router){}

  ngOnInit(): void {
    this.data.push({id : 1, name : 'John'})
    this.data.push({id : 2, name : 'Doe'})
  }

  create(){
    this.router.navigateByUrl('users/new')
  }
  
  view(id? : number){
    this.router.navigateByUrl(`users/${id}`)
  }
}
